package herancadependecia;

import com.sun.corba.se.impl.orbutil.CorbaResourceUtil;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.cell.PropertyValueFactory;

public class FXMLDocumentController implements Initializable {

    @FXML
    private TableView<Pessoa> tabela;
    @FXML
    private TableColumn<Pessoa, String> nomeCol;
    @FXML
    private TableColumn<Pessoa, Integer> idadeCol;
    @FXML
    private TableColumn<Pessoa, String> enderecoCol;
    @FXML
    private TableColumn<Pessoa, String> funcaoCol;

    //observableList
    private ObservableList<Pessoa> pessoasList;
    ArrayList<Pessoa> pessoa = new ArrayList<Pessoa>();

    @FXML
    private Button detalhesBT;
    @FXML
    private Button deletarBT;
    @FXML
    private Button editarBT;
    @FXML
    private TextField idadeTF;
    @FXML
    private TextField nomeTF;
    @FXML
    private TextField enderecoTF;
    @FXML
    private ToggleGroup atributos;
    @FXML
    private TextField semestreTF;
    @FXML
    private TextField cursoTF;
    @FXML
    private TextField setorTF;
    @FXML
    private TextField funcaoTF;
    @FXML
    private TextField salarioAdmTF;
    @FXML
    private TextField disciplinaTF;
    @FXML
    private TextField salarioProfTF;
    @FXML
    private Label detalhesLB;
    @FXML
    private Label label;
    @FXML
    private Label label1;
    @FXML
    private Label label2;
    @FXML
    private Label label3;
    @FXML
    private RadioButton alunoRB;
    @FXML
    private RadioButton funcAdmRB;
    @FXML
    private RadioButton professorRB;
    @FXML
    private Label semestreLB;
    @FXML
    private Label cursoLB;
    @FXML
    private Label setorLB;
    @FXML
    private Label funcaoLB;
    @FXML
    private Label salarioAdmLB;
    @FXML
    private Label salarioProfLB;
    @FXML
    private Label disciplinaLB;
    @FXML
    private Button cadastrarBT;
    @FXML
    private Label mensagem;
    @FXML
    private Label label21;
    @FXML
    private TextField codigoTF;
    @FXML
    private TableColumn<?, ?> codigoCol;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        pessoasList = tabela.getItems();
        pessoasList.clear();
        nomeCol.setCellValueFactory(new PropertyValueFactory<>("nome"));
        idadeCol.setCellValueFactory(new PropertyValueFactory<>("idade"));
        enderecoCol.setCellValueFactory(new PropertyValueFactory<>("endereco")); 
        codigoCol.setCellValueFactory(new PropertyValueFactory<>("codigo"));
        funcaoCol.setCellValueFactory(new PropertyValueFactory<>("tipo"));
        Aluno.getAll().forEach((a) -> {
            pessoasList.add(a);
        });
        Professor.getAll().forEach((a) -> {
            pessoasList.add(a);
        });
        FuncAdm.getAll().forEach((a) -> {
            pessoasList.add(a);
        });

    }

    @FXML
    private void detalhes(ActionEvent event) {
        if(tabela.getSelectionModel().getSelectedItem() instanceof Aluno){
            Aluno a = (Aluno) tabela.getSelectionModel().getSelectedItem();
            detalhesLB.setText("semestre:"+a.getSemestre()+"\n curso:"+a.getCurso());
            
        }
        if(tabela.getSelectionModel().getSelectedItem() instanceof FuncAdm){
            FuncAdm adm = (FuncAdm) tabela.getSelectionModel().getSelectedItem();
            detalhesLB.setText("funçao::"+adm.getFuncao()+"\n Setor::"+adm.getSetor()+"\n salario:"+String.valueOf(adm.getSalarioAdm()));
        }
        if(tabela.getSelectionModel().getSelectedItem() instanceof Professor){
            Professor pf = (Professor) tabela.getSelectionModel().getSelectedItem();
            detalhesLB.setText("Disciplina:"+pf.getDiciplina()+"\n "+"\n Salario:"+String.valueOf(pf.getSalarioProf()));
        }
        limparCampos();
        cadastrarBT.setDisable(false);
        
    }

    @FXML
    private void deletar(ActionEvent event) {
         pessoasList.remove(tabela.getSelectionModel().getSelectedItem());
         tabela.getSelectionModel().getSelectedItem().delete();
         limparCampos();
         cadastrarBT.setDisable(false);
         
    }

    @FXML
    private void editar(ActionEvent event) {
        
        
        tabela.getSelectionModel().getSelectedItem().setNome(nomeTF.getText());
        tabela.getSelectionModel().getSelectedItem().setIdade(Integer.valueOf(idadeTF.getText()));
        tabela.getSelectionModel().getSelectedItem().setEndereco(enderecoTF.getText());
        tabela.getSelectionModel().getSelectedItem().setCodigo(Integer.valueOf(codigoTF.getText()));
        
        if(tabela.getSelectionModel().getSelectedItem() instanceof Aluno){
            Aluno a = (Aluno) tabela.getSelectionModel().getSelectedItem();
            a.setSemestre(semestreTF.getText());
            a.setCurso(cursoTF.getText());
        }
        if(tabela.getSelectionModel().getSelectedItem() instanceof FuncAdm){
            FuncAdm adm = (FuncAdm) tabela.getSelectionModel().getSelectedItem();
            adm.setFuncao(funcaoTF.getText());
            adm.setSalarioAdm(Integer.parseInt(salarioAdmTF.getText()));
            adm.setSetor(setorTF.getText());
        }
        if(tabela.getSelectionModel().getSelectedItem() instanceof Professor){
            Professor pf = (Professor) tabela.getSelectionModel().getSelectedItem();
            pf.setDiciplina(disciplinaTF.getText());
            pf.setSalarioProf(Integer.parseInt(salarioProfTF.getText()));
        }
        limparCampos();
        tabela.refresh();
    }

    @FXML
    private void Cadastrar(ActionEvent event) {

        //Pessoa p = new Pessoa();

        if (alunoRB.isSelected()) {
            
            setorTF.setVisible(true);
            Aluno a = new Aluno();
            a.setSemestre(semestreTF.getText());
            a.setCurso(cursoTF.getText());
            a.setIdade(Integer.parseInt(idadeTF.getText()));
            a.setEndereco(enderecoTF.getText());
            a.setNome(nomeTF.getText());
            a.setCodigo(Integer.parseInt(codigoTF.getText()));
            a.setTipo("Aluno");
            pessoasList.add(a);
            a.insert();
            
            
        } else if (funcAdmRB.isSelected()) {
            
            FuncAdm adm = new FuncAdm();
            adm.setSetor(setorTF.getText());
            adm.setFuncao(funcaoTF.getText());
            adm.setSalarioAdm(Integer.parseInt(salarioAdmTF.getText()));
            adm.setIdade(Integer.parseInt(idadeTF.getText()));
            adm.setEndereco(enderecoTF.getText());
            adm.setNome(nomeTF.getText());
            adm.setCodigo(Integer.parseInt(codigoTF.getText()));
            adm.setTipo("funcionario ADM");
            adm.insert();
            pessoasList.add(adm);
            

        } else if (professorRB.isSelected()) {
            
            Professor pf = new Professor();
            pf.setDiciplina(disciplinaTF.getText());
            pf.setSalarioProf(Integer.parseInt(salarioProfTF.getText()));
            pf.setIdade(Integer.parseInt(idadeTF.getText()));
            pf.setEndereco(enderecoTF.getText());
            pf.setNome(nomeTF.getText());
            pf.setCodigo(Integer.parseInt(codigoTF.getText()));
            pf.setTipo("professor");
            pf.insert();
            pessoasList.add(pf);
        }
       
        mensagem.setText("Dado Foi Cadastrado Corretamente");
        limparCampos();
        
    }

    @FXML
    private void marcarAluno(ActionEvent event) {
        if(alunoRB.isSelected()){
            desabilitarTudo();
            semestreTF.setDisable(false);
            semestreLB.setDisable(false);
            cursoTF.setDisable(false);
            cursoLB.setDisable(false);
        }if(funcAdmRB.isSelected()){
            desabilitarTudo();
            setorLB.setDisable(false);
            funcaoTF.setDisable(false);
            funcaoLB.setDisable(false);
            salarioAdmTF.setDisable(false);
            salarioAdmLB.setDisable(false);
            setorTF.setDisable(false);
        } if(professorRB.isSelected()){
            desabilitarTudo();
            salarioProfLB.setDisable(false);
            salarioProfTF.setDisable(false);
            disciplinaTF.setDisable(false);
            disciplinaLB.setDisable(false);
        }
    }
    
    @FXML
    private void selecionado(javafx.scene.input.MouseEvent event) {
        if(tabela.getSelectionModel().getSelectedItem() != null){
            deletarBT.setDisable(false);
            editarBT.setDisable(false);
            cadastrarBT.setDisable(true);
            
            
            Pessoa pessoaSelecionado = tabela.getSelectionModel().getSelectedItem();
            nomeTF.setText(pessoaSelecionado.getNome());
            idadeTF.setText(String.valueOf(pessoaSelecionado.getIdade()));
            enderecoTF.setText(pessoaSelecionado.getEndereco());
            codigoTF.setText(String.valueOf(pessoaSelecionado.getCodigo()));
            
            if(tabela.getSelectionModel().getSelectedItem() instanceof Aluno){
                Aluno a = (Aluno) tabela.getSelectionModel().getSelectedItem();
                semestreTF.setText(a.getSemestre());
                cursoTF.setText(a.getCurso());
        }
            if(tabela.getSelectionModel().getSelectedItem() instanceof FuncAdm){
                FuncAdm adm = (FuncAdm) tabela.getSelectionModel().getSelectedItem();
                setorTF.setText(adm.getSetor());
                funcaoTF.setText(adm.getFuncao());
                salarioAdmTF.setText(String.valueOf(adm.getSalarioAdm()));
            
            }
            if(tabela.getSelectionModel().getSelectedItem() instanceof Professor){
                Professor pf = (Professor) tabela.getSelectionModel().getSelectedItem();
                salarioProfTF.setText(String.valueOf(pf.getSalarioProf()));
                disciplinaTF.setText(pf.getDiciplina());
            }
        }
    }

     
    public void desabilitarTudo(){
        setorTF.setDisable(true);
            setorLB.setDisable(true);
            funcaoTF.setDisable(true);
            funcaoLB.setDisable(true);
            salarioAdmTF.setDisable(true);
            salarioAdmLB.setDisable(true);
            salarioProfLB.setDisable(true);
            salarioProfTF.setDisable(true);
            disciplinaTF.setDisable(true);
            disciplinaLB.setDisable(true);
            semestreTF.setDisable(true);
            semestreLB.setDisable(true);
            cursoTF.setDisable(true);
            cursoLB.setDisable(true);
    
    }
    public void atualizarDados(){
  //  pessoasList = tabela.getItems();
//        pessoasList.clear();
//        nomeCol.setCellValueFactory(new PropertyValueFactory<>("nome"));
//        idadeCol.setCellValueFactory(new PropertyValueFactory<>("idade"));
//        enderecoCol.setCellValueFactory(new PropertyValueFactory<>("endereco")); 
//        codigoCol.setCellValueFactory(new PropertyValueFactory<>("codigo"));
//        funcaoCol.setCellValueFactory(new PropertyValueFactory<>("tipo"));
//        
////        for(Aluno a : Aluno.getAll()){
////
////           pessoasList.add(a);
////        } 
////        for(FuncAdm adm : FuncAdm.getAll()){
////
////           pessoasList.add(adm);
////        } 
////        for(Professor pf : Professor.getAll()){
////
////           pessoasList.add(pf);
////        } 
//
//        Aluno.getAll().forEach((a) -> {
//            pessoasList.add(a);
//        });
//        Professor.getAll().forEach((adm) -> {
//            pessoasList.add(adm);
//        });
//        FuncAdm.getAll().forEach((pf) -> {
//            pessoasList.add(pf);
//        });

        
    }
    public void limparCampos(){
        nomeTF.setText("");
        idadeTF.setText("");
        enderecoTF.setText("");
        semestreTF.setText("");
        cursoTF.setText("");
        funcaoTF.setText("");
        salarioAdmTF.setText("");
        setorTF.setText("");
        salarioProfTF.setText("");
        disciplinaTF.setText("");
        codigoTF.setText("");
    }
}
