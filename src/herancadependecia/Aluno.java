package herancadependecia;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;


public class Aluno extends Pessoa{
    
    private String semestre;
    private String curso;
    
    public boolean insert() {
	Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement preparedStatement = null;

        String insertTableSQL = "INSERT INTO OO_heranca_exo (nome, idade , endereco,semestre, curso, codigo, funcao) VALUES (?,?,?,?,?,?,?)";
        

        try {
            preparedStatement = dbConnection.prepareStatement(insertTableSQL);
            preparedStatement.setString(1, getNome());
            preparedStatement.setInt(2,getIdade() );
            preparedStatement.setString(3,getEndereco());
            preparedStatement.setString(4,this.semestre);
            preparedStatement.setString(5,this.curso);
            preparedStatement.setInt(6,getCodigo());
            preparedStatement.setString(7,"aluno");
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            
            e.printStackTrace();
        }finally{
            c.desconecta(); 
        }        
        return true;
    }
    
    
    public static ArrayList<Aluno> getAll() {
        String selectSQL = "SELECT * FROM OO_heranca_exo where funcao='Aluno'";
        ArrayList<Aluno> lista = new ArrayList<>();
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        Statement st;
        try {
            
            st = dbConnection.createStatement();
            ResultSet rs = st.executeQuery(selectSQL);
            while (rs.next()) {
                Aluno a = new Aluno();
                a.setNome(rs.getString("nome"));
                a.setIdade(rs.getInt("idade"));
                a.setEndereco(rs.getString("endereco"));
                a.setSemestre(rs.getString("semestre"));
                a.setCurso(rs.getString("curso"));
                a.setCodigo(rs.getInt("codigo"));
                a.setTipo("Aluno");
                
                lista.add(a);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }finally{
            c.desconecta(); 
        }   

        return lista;
    }
    
    
    public boolean update() {
      	Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement ps = null;

        String insertTableSQL = "UPDATE OO_heranca_exo SET nome =?, idade=?, endereco=?, semestre=?, curso=?,funcao=? WHERE codigo = ?";
        

        try {
            ps = dbConnection.prepareStatement(insertTableSQL);
            ps.setString(1, getNome());
            ps.setInt(6, getCodigo());
            ps.setInt(2, getIdade());
            ps.setString(3,getEndereco());
            ps.setString(4, this.semestre);
            ps.setString(5, this.curso);
            ps.setString(7, getTipo());
          
            ps.executeUpdate();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }finally{
            c.desconecta(); //Boa pratica
        }        
        return true;
    }
    
    public boolean delete() {
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement ps = null;

        String insertTableSQL = "DELETE FROM OO_heranca_exo WHERE codigo = ?";        

        try {
            ps = dbConnection.prepareStatement(insertTableSQL);
            ps.setInt(1, getCodigo());
          
            ps.executeUpdate();
            
        } catch (SQLException e) {
            
            e.printStackTrace();
        }finally{
            
            c.desconecta();
        }        
        return true;
        
    }

    public String getSemestre() {
        return semestre;
    }

    public void setSemestre(String semestre) {
        this.semestre = semestre;
    }

    public String getCurso() {
        return curso;
    }

    public void setCurso(String curso) {
        this.curso = curso;
    }
    
    
    
}
