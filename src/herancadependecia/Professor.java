package herancadependecia;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class Professor extends Funcionario{
    
    private String diciplina;
    private int salarioProf;
    
    public boolean insert() {
	Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement preparedStatement = null;

        String insertTableSQL = "INSERT INTO OO_heranca_exo (nome, idade , endereco, salario_prof, disciplina, codigo, funcao) VALUES (?,?,?,?,?,?,?)";
        

        try {
            preparedStatement = dbConnection.prepareStatement(insertTableSQL);
            preparedStatement.setString(1, getNome());
            preparedStatement.setInt(2,getIdade() );
            preparedStatement.setString(3,getEndereco());
            preparedStatement.setInt(4,this.salarioProf);
            preparedStatement.setString(5,this.diciplina);
            preparedStatement.setInt(6,getCodigo());
            preparedStatement.setString(7,getTipo());
            
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            
            e.printStackTrace();
        }finally{
            c.desconecta(); 
        }        
        return true;
    }
    
    public static ArrayList<Professor> getAll() {
        String selectSQL = "SELECT * FROM OO_heranca_exo where funcao='Professor'";
        ArrayList<Professor> lista = new ArrayList<>();
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        Statement st;
        try {
            
            st = dbConnection.createStatement();
            ResultSet rs = st.executeQuery(selectSQL);
            while (rs.next()) {
                Professor p = new Professor();
                p.setNome(rs.getString("nome"));
                p.setIdade(rs.getInt("idade"));
                p.setEndereco(rs.getString("endereco"));
                p.setSalario(rs.getInt("salario_prof"));
                p.setDiciplina(rs.getString("disciplina"));
                p.setCodigo(rs.getInt("codigo"));
                p.setTipo("Professor");
                lista.add(p);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }finally{
            c.desconecta(); 
        }   

        return lista;
    }
    
    public boolean update() {
      	Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement ps = null;

        String insertTableSQL = "UPDATE OO_heranca_exo SET nome =?, idade=?, endereco=?, salario_prof=?, disciplina=?,funcao=? WHERE codigo = ?";
        

        try {
            ps = dbConnection.prepareStatement(insertTableSQL);
            ps.setString(1, getNome());
            ps.setInt(6, getCodigo());
            ps.setInt(2, getIdade());
            ps.setString(3,getEndereco());
            ps.setInt(4, this.salarioProf);
            ps.setString(5, diciplina);
            ps.setString(7, getTipo());
          
            ps.executeUpdate();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }finally{
            c.desconecta(); //Boa pratica
        }        
        return true;
    }
    
    public boolean delete() {
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement ps = null;

        String insertTableSQL = "DELETE FROM OO_heranca_exo WHERE codigo = ?";        

        try {
            ps = dbConnection.prepareStatement(insertTableSQL);
            ps.setInt(1, getCodigo());
          
            ps.executeUpdate();
            
        } catch (SQLException e) {
            
            e.printStackTrace();
        }finally{
            
            c.desconecta();
        }        
        return true;
        
    }

    

    public String getDiciplina() {
        return diciplina;
    }

    public void setDiciplina(String diciplina) {
        this.diciplina = diciplina;
    }

    public int getSalarioProf() {
        return salarioProf;
    }

    public void setSalarioProf(int salarioProf) {
        this.salarioProf = salarioProf;
    }
    
    
    
}
