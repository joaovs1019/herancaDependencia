package herancadependecia;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;


public class FuncAdm extends Funcionario{
    
    private String setor;
    private String funcao;
    private int salarioAdm;
    
    public boolean insert() {
	Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement preparedStatement = null;

        String insertTableSQL = "INSERT INTO OO_heranca_exo (nome, idade , endereco, salario_func_adm, funcao_func_adm, setor, codigo,funcao) VALUES (?,?,?,?,?,?,?,?)";
        

        try {
            preparedStatement = dbConnection.prepareStatement(insertTableSQL);
            preparedStatement.setString(1, getNome());
            preparedStatement.setInt(2,getIdade() );
            preparedStatement.setString(3,getEndereco());
            preparedStatement.setInt(4,this.salarioAdm);
            preparedStatement.setString(5,this.funcao);
            preparedStatement.setString(6,this.setor);
            preparedStatement.setInt(7,getCodigo());
            preparedStatement.setString(8,getTipo());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            
            e.printStackTrace();
        }finally{
            c.desconecta(); 
        }        
        return true;
    }
    
    
    public static ArrayList<FuncAdm> getAll() {
        String selectSQL = "SELECT * FROM OO_heranca_exo where funcao='FuncAdm'";
        ArrayList<FuncAdm> lista = new ArrayList<>();
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        Statement st;
        try {
            
            st = dbConnection.createStatement();
            ResultSet rs = st.executeQuery(selectSQL);
            while (rs.next()) {
                FuncAdm adm = new FuncAdm();
                adm.setNome(rs.getString("nome"));
                adm.setIdade(rs.getInt("idade"));
                adm.setEndereco(rs.getString("endereco"));
                adm.setSetor(rs.getString("setor"));
                adm.setFuncao(rs.getString("funcao_func_adm"));
                adm.setSalarioAdm(rs.getInt("salario_func_adm"));
                adm.setCodigo(rs.getInt("codigo"));
                adm.setTipo("FuncAdm");
                lista.add(adm);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }finally{
            c.desconecta(); 
        }   

        return lista;
    }
    
    
     public boolean update() {
      	Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement ps = null;

        String insertTableSQL = "UPDATE OO_heranca_exo SET nome =?, idade=?, endereco=?, setor=?, funcao_func_adm=?, salario_func_adm,funcao=? WHERE codigo = ?";
        

        try {
            ps = dbConnection.prepareStatement(insertTableSQL);
            ps.setString(1, getNome());
            ps.setInt(7, getCodigo());
            ps.setInt(2, getIdade());
            ps.setString(3,getEndereco());
            ps.setString(4, this.setor);
            ps.setString(5, this.funcao);
            ps.setInt(6, this.salarioAdm);
            ps.setString(8, getTipo());
          
            ps.executeUpdate();
        } catch (SQLException e) {
            
            e.printStackTrace();
        }finally{
            c.desconecta(); 
        }        
        return true;
    }
     
     
     public boolean delete() {
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement ps = null;

        String insertTableSQL = "DELETE FROM OO_heranca_exo WHERE codigo = ?";        

        try {
            ps = dbConnection.prepareStatement(insertTableSQL);
            ps.setInt(1, getCodigo());
          
            ps.executeUpdate();
            
        } catch (SQLException e) {
            
            e.printStackTrace();
        }finally{
            
            c.desconecta();
        }        
        return true;
        
    }

    public String getSetor() {
        return setor;
    }

    public void setSetor(String setor) {
        this.setor = setor;
    }

    public String getFuncao() {
        return funcao;
    }

    public void setFuncao(String funcao) {
        this.funcao = funcao;
    }

    public int getSalarioAdm() {
        return salarioAdm;
    }

    public void setSalarioAdm(int salarioAdm) {
        this.salarioAdm = salarioAdm;
    }
    
    
}
